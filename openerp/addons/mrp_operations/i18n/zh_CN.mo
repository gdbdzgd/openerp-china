��    h      \  �   �      �  
   �  �  �  �  �
    �     �     �     �     �     �  	   �  a   �     H     W     \  
   r  
   }     �     �     �     �     �     �     �     �     �     �     �     �     �               ,     /     =  J   I  I   �  @   �          +     ;     K     P  4   d     �     �     �     �  P   �     /     O     n  
   �  
   �     �     �     �     �     �     �     �     �  
             #     =     O     e     }     �     �     �     �     �     �     �     �     �  	             %     ,  
   2     =     K     ^     f  
   m  E   x     �     �     �     �     �            
   #     .     B     T     `     x     �     �  �  �     N  m  W  {  �  �  A     �     �            	      	   *  H   4  	   }     �     �  	   �  	   �  	   �     �     �     �  	   �     �     �     �  	   �     	  	        #     6  	   F     P     l  	   o  	   y  ?   �  3   �  0   �     (     /     ?     O  	   V  +   `     �     �     �     �  C   �                0     F     M     Z     a  	   n     x     �     �     �     �     �     �     �     �     �     �                 *      =      J      W      d      z      �      �      �      �      �      �      �      �      �      �      �      !  ?   !     Q!     a!     n!     {!     �!     �!     �!     �!     �!     �!     �!     �!     �!      "  	   "        F   +   $   c       Q   h   "       \   ,              (   >       =   #       9       ]   N          V      M   &   J   ^   d   D   -   *   
   _      b              W               <   5          3       4   E                  P   f      O      X   :   ?   8   U   [   Z         7   g   I       C   )                            	         0   H   '                  R       a   6   B   A      K   G   1   /   Y          L   `   %          S          T      .       !   e                 ;              2   @              # of Lines * When a work order is created it is set in 'Draft' status.
* When user sets work order in start mode that time it will be set in 'In Progress' status.
* When work order is in running mode, during that time if user wants to stop or to make changes in order then can set in 'Pending' status.
* When the user cancels the work order it will be set in 'Canceled' status.
* When order is completely processed that time it is set in 'Finished' status. <p class="oe_view_nocontent_create">
            Click to start a new work order.
          </p><p>
            To manufacture or assemble products, and use raw materials and
            finished products you must also handle manufacturing operations.
            Manufacturing operations are often called Work Orders. The various
            operations will have different impacts on the costs of
            manufacturing and planning depending on the available workload.
          </p>
         <p class="oe_view_nocontent_create">
            Click to start a new work order. 
          </p><p>
            Work Orders is the list of operations to be performed for each
            manufacturing order. Once you start the first work order of a
            manufacturing order, the manufacturing order is automatically
            marked as started. Once you finish the latest operation of a
            manufacturing order, the MO is automatically done and the related
            products are produced.
          </p>
         Actual Production Date Calendar View Cancel Cancel Order Canceled Cancelled Check this to be able to move independently all production orders, without moving dependent ones. Children Moves Code Confirmed Work Orders Created by Created on Current Current Production Date Delay Done Draft Duration End Date Error! Finish Order Finished Free Serialisation Future Work Orders Group By Hours by Work Center ID In Production In Progress In order to Finish the operation, it must be in the Start or Resume state! In order to Pause the operation, it must be in the Start or Resume state! In order to Resume the operation, it must be in the Pause state! Information Last Updated by Last Updated on Late Manufacturing Order Manufacturing order cannot be started in state "%s"! Month Planned No operation to cancel. Operation Codes Operation Name Operation has already started! You can either Pause/Finish/Cancel the operation. Operation is Already Cancelled! Operation is already finished! Operation is not started yet! Operations Order Date Pause Pause Work Order Pending Planned Date Planned Month Product Product Qty Product to Produce Production Production Operation Production Operation Code Production Status Production Workcenter Production started late Qty Quantity Produced Ready to Produce Resume Resume Work Order Scheduled Date Scheduled Date by Month Scheduled Month Search Search Work Orders Set Draft Set to Draft Sorry! Start Start Date Start Working Start/Stop Barcode Started Status Stock Move The elapsed time between operation start and stop in this Work Center Total Cycles Total Hours Unit of Measure Waiting Goods Work Center Work Centers Work Centers Barcode Work Order Work Order Analysis Work Order Report Work Orders Work Orders By Resource Work Orders Planning Working Hours Workload Project-Id-Version: openobject-addons
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-23 16:28+0000
PO-Revision-Date: 2014-09-30 12:30+0800
Last-Translator: 保定-粉刷匠 <992102498@qq.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-03-08 05:51+0000
X-Generator: Poedit 1.6.9
 # 明细 * 工单被创建时，“草稿“状态。
* 从工单被用户进入开始模式后，设为”在进行中“模式。
* 当工单是运行模式，在此期间，如果用户要停止或作出改变，可设置为”暂停“状态。
* 当用户取消工单，设置为”取消状态“。
* 从工单被完成的时候开始，设置为”完成“状态。 <p class="oe_view_nocontent_create">
            单击启动一个新的工单。
          </p><p>
            要生产或者组装产品，并且使用原料，也必须处理生产操作完成产品。
            生产操作经常被叫做工单。
            各种操作对生产成本有不同的影响，计划取决于可用的工作能力。
          </p>
         <p class="oe_view_nocontent_create">
            单击创建一个新的工单。
          </p><p>
            工单是每个生产单要被执行的操作列表。
            一旦你启动生产单的第一个工单，生产单将被自动标记为启动。
            一旦你完成了生产单最后一个操作，生产单被自动完成， 
            相关的产品也被制造完成。
          </p>
         实际生产日期 日历视图 取消 取消订单 已作废 已取消 选中这里就不会按工作中心工作时间重排生产单日期。 子调拨 代码 已确认的工单 创建人 创建在 当前的 当前产品 日期 延期 已完成 草稿 时长 结束日期 错误！ 生产单完成 已完成 自由排序生产 待开工工单 分组按 按工作中心工时合计 ID 生产中 进行中 要完成工序，它必须处于开始或重新开始状态。 要暂停工序，必须在开始或重启状态。 要重启这个工序，必须在暂停状态。 信息 最后更新被 最后更新在 延迟 生产单 处于"%s"状态的生产单不能开始！ 计划月份 没有操作被取消 工序代码 工序名称 操作已经开始！你可以 暂停/完成/取消 这个操作。 工序已经取消 工序已完成 工序还未开始！ 工序 单据日期 暂停 暂停工单 等待中 计划日期 计划月份 产品 产品数量 待生产的产品 生产 生产工序 工序代码 生产状态 生成工作中心 未准时开始生产 数量 已生产的产品 生产准备就绪 重新开始 重启工单 计划日期 按月的计划日期 计划月份 查询 查找工单： 设为草稿 设为草稿 抱歉! 开始 开始日期 开始工作 开始/停止 条码 开始 状态 库存调拨 工序在这个工作中心从开始到结束的执行周期。 总加工次数 总小时数 计量单位 等待原料 工作中心 工作中心 工作中心条码 工单 工单分析 工单报表 工单 工单（按资源） 工单计划 工时(小时) 工作量 